/**
 * @mixin
 */

import ParseErrorMixin from './ParseErrorMixin'

export default {
  mixins: [
    ParseErrorMixin
  ],
  props: {
    apiPasswordResetUrl: {
      type: String,
      default: '/api/v1/auth/password/reset/'
    }
  },
  methods: {
    performApiPasswordReset (email) {
      this.$emit('performingApiPasswordReset', this.name)

      let params = {
        email: email
      }

      this.$http.post(
        this.apiPasswordResetUrl,
        params,
        {}
      ).then((response) => {
        this.$emit('apiPasswordResetResponse', response.data)
      }).catch((error) => {
        this.$emit('apiPasswordResetError', this.parseError(error))
      })
    },
    performApiPasswordResetP (email) {
      return new Promise((resolve, reject) => {
        this.$on('apiPasswordResetResponse', (response) => {
          resolve(response)
        })
        this.$on('apiPasswordResetError', (error) => {
          reject(error)
        })

        this.performApiPasswordReset(email)
      })
    }
  }
}
