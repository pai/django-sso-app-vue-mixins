/*
Fingerprintjs2 Vue wrapper

// https://github.com/Valve/fingerprintjs2/blob/master/README.md
*/

import Fingerprint2 from 'fingerprintjs2'

export default {
  props: {
    fingerprintOptions: {
      type: Object,
      default () {
        return {}
      }
    }
  },
  data () {
    return {
      fingerprintjs2: null,
      browserFingerprint: 'undefined'
    }
  },
  beforeCreate () {
    this.$on('borwserFingerprintChanged', (fp) => {
        this.browserFingerprint = fp
    })
  },
  created () {
    this.fingerprintjs2 = new Fingerprint2.get(this.fingerprintOptions, (components) => {
      let values = components.map(function (component) { return component.value })
      let murmur = Fingerprint2.x64hash128(values.join(''), 31)

      if (murmur) {
        this.$emit('borwserFingerprintChanged', murmur)
      }
    })
  }
}
