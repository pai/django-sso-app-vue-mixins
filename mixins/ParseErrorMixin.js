/**
 * @mixin
 */

import GettextMixin from '@paiuolo/pai-vue-gettext/mixins/GettextMixin'

export default {
  mixins: [
    GettextMixin
  ],
  data () {
    return {
      backendGenericErrorKeys: ['__all__'],
      backendPermittedErrors: '400,401,403,404',
      backendErrorMessage: this.gettext('Something happened in setting up the request that triggered an Error. Please contact support.'),
      backendUnreacheableMessage: this.gettext('Server is unreacheable, please check your connection or try later.')
    }
  },
  methods: {
    parseError (error, disableLog) {
      let message = ''

      if (!disableLog) {
       console.error('DjangoSsoAppError', error)
      }

      if (error.response) {
        let status = error.response.status

        if (this.backendPermittedErrors.indexOf(status) > -1) {
          let responseData = error.response.data

          if (responseData) {

            if (typeof responseData === 'object') {
              if (responseData.hasOwnProperty('html') && responseData['html'].length) {
                responseData = JSON.parse(responseData['html'])
              }

              for (let property in responseData) {
                if (responseData.hasOwnProperty(property)) {
                  if (this.backendGenericErrorKeys.indexOf(property) > -1) {
                    message += property + ': ' + responseData[property] + '. '
                  }
                }
              }

              return message.trim()

            } else {
              return responseData
            }
          } else {
            return this.backendErrorMessage
          }
        } else {
         return this.backendErrorMessage
        }
      } else {
        return this.backendUnreacheableMessage
      }
    }
  }
}
