/**
 * @mixin
 */

import ParseErrorMixin from './ParseErrorMixin'


export default {
  mixins: [
    ParseErrorMixin
  ],
  props: {
    fetchEmailsUrl: {
      type: String,
      default: '/api/v1/auth/email/'
    }
  },
  methods: {
    fetchEmails () {
      this.$emit('performingFetchEmails', this.name)

      this.$http.get(
        this.fetchEmailsUrl,
        {
          withCredentials: true
        }
      ).then((response) => {
        this.$emit('emailsFetched', response.data)
      }).catch((error) => {
        this.$emit('errorFetchingEmails', this.parseError(error))
      })
    },
    fetchEmailsP () {
      return new Promise((resolve, reject) => {
        this.$on('emailsFetched', (response) => {
          resolve(response)
        })
        this.$on('errorFetchingEmails', (error) => {
          reject(error)
        })

        this.fetchEmails()
      })
    }
  }
}
