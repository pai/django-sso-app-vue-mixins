/**
 * @mixin
 */
import ParseErrorMixin from './ParseErrorMixin'


export default {
  mixins: [
    ParseErrorMixin
  ],
  props: {
    groupsUrl: {
      type: String,
      default: '/api/v1/auth/groups/'
    }
  },
  methods: {
    fetchGroups () {
      this.$emit('performingFetchGroups')

      this.$http.get(
        this.groupsUrl,
        {
          withCredentials: true
        }
      ).then((response) => {
        this.$emit('fetchGroupsResponse', response.data)
      }).catch((error) => {
        this.$emit('fetchGroupsError', this.parseError(error))
      })
    },
    fetchGroupsP () {
      return new Promise((resolve, reject) => {
        this.$on('fetchGroupsResponse', (response) => {
          resolve(response)
        })
        this.$on('fetchGroupsError', (error) => {
          reject(error)
        })

        this.fetchGroups()
      })
    }
  }
}
