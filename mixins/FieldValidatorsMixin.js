/**
 * @mixin
 */

export default {
  methods: {
    validateEmail (s) {
      return s && /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(.\w{2,3})+$/.test(s)
    },
    validateUsername (s) {
      return s && s.length && /^[a-zA-Z0-9.!#$%&@*+/=?^_`{|}~-]+$/.test(s)
    },
    validatePassword (s) {
      return s && (s.length > 7) && /^[a-zA-Z0-9.!#$%&@'*+/=?^_`{|}~-]+$/.test(s)
    },
    validateFirstName (s) {
      return s && s.length && /^[a-zA-Z ]+/.test(s)
    },
	validateLastName (s) {
      if (s && s.length) {
        return this.validateFirstName(s)
      } else {
        return true
      }
    }
  },
  data () {
    return {
      emailRules: [
        (v) => {
          return this.validateEmail(v) || ''
        }
      ],
      usernameRules: [
        (v) => {
          return this.validateUsername(v) || ''
        }
      ],
      passwordRules: [
        (v) => {
          return this.validatePassword(v) || ''
        }
      ],
      userFirstNameRules: [
        (v) => {
          return this.validateFirstName(v) || ''
        }
      ],
      userLastNameRules: [
        (v) => {
          return this.validateFirstName(v) || ''
        }
      ]
    }
  }
}
