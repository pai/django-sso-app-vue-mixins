/**
 * @mixin
 */
import ParseErrorMixin from './ParseErrorMixin'


export default {
  mixins: [
    ParseErrorMixin
  ],
  props: {
    fetchBackendStatsUrl: {
      type: String,
      default: '/api/v1/_stats/'
    }
  },
  methods: {
    fetchBackendStats () {
      this.$emit('performingFetchBackendStats', this.name)

      this.$http.get(
        this.fetchBackendStatsUrl,
        {
          withCredentials: true
        }
      ).then((response) => {
        this.$emit('backendStatsFetched', response.data)
      }).catch((error) => {
        this.$emit('errorFetchingBackendStats', this.parseError(error))
      })
    },
    fetchBackendStatsP () {
      return new Promise((resolve, reject) => {
        this.$on('backendStatsFetched', (response) => {
          resolve(response)
        })
        this.$on('errorFetchingBackendStats', (error) => {
          reject(error)
        })

        this.fetchBackendStats()
      })
    }
  }
}
