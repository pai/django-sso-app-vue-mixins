/**
 * @mixin
 */

import ParseErrorMixin from './ParseErrorMixin'


export default {
  mixins: [
    ParseErrorMixin
  ],
  props: {
    fetchServicesUrl: {
      type: String,
      default: '/api/v1/auth/services/'
    }
  },
  methods: {
    fetchServices () {
      this.$emit('performingFetchServices', this.name)

      this.$http.get(
        this.fetchServicesUrl,
        {
          withCredentials: true
        }
      ).then((response) => {
        this.$emit('servicesFetched', response.data)
      }).catch((error) => {
        this.$emit('errorFetchingServices', this.parseError(error))
      })
    },
    fetchServicesP () {
      return new Promise((resolve, reject) => {
        this.$on('servicesFetched', (response) => {
          resolve(response)
        })
        this.$on('errorFetchingServices', (error) => {
          reject(error)
        })

        this.fetchServices()
      })
    }
  }
}
