/**
 * @mixin
 * Check if login is either by username or by email.
*/
import GettextMixin from '@paiuolo/pai-vue-gettext/mixins/GettextMixin'

import FieldValidatorsMixin from './FieldValidatorsMixin'

export default {
  mixins: [
    GettextMixin,
    FieldValidatorsMixin
  ],
  data () {
    return {
      invalidCredentialsError: this.gettext('Invalid credentials.'),
      invalidEmailError: this.gettext('Invalid email.'),
      invalidUsernameError: this.gettext('Invalid username.')
    }
  },
  methods: {
    getCredentialsType (string) {
      // console.log('VALUTO CREDENZIALI', string)
      if (this.validateEmail(string)) {
        return 'email'
      } else if (this.validateUsername(string)) {
        return 'username'
      } else {
        return null
      }
    },
    credentialsTypeIsValid (string, credentialsType) {
      if (credentialsType) {
        if (['email', 'username', 'login'].indexOf(credentialsType) < 0) {
          return false
        }
      } else {
        credentialsType = this.getCredentialsType(string)
      }

      if (credentialsType === 'email') {
        return this.validateEmail(string)
      } else if (credentialsType === 'username') {
        return this.validateUsername(string)
      } else {
        return (this.validateEmail || this.validateUsername)
      }

      return false
    }
  }
}
