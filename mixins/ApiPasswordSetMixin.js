/**
 * @mixin
 */

import ParseErrorMixin from './ParseErrorMixin'

export default {
  mixins: [
    ParseErrorMixin
  ],
  props: {
    apiPasswordSetUrl: {
      type: String,
      default: '/api/v1/auth/password/set/'
    }
  },
  methods: {
    apiPasswordSetParseError (error) {
      let parsedError = this.parseError(error)
      let msg = parsedError

      if (typeof parsedError === 'object') {
        msg = this.gettext('Check password.')
      }

      this.$emit('apiPasswordSetError', msg)
    },
    performApiPasswordSet (obj) {
      this.$emit('performingApiPasswordSet')

      this.$http.post(
        this.apiPasswordSetUrl,
        obj,
        {
          withCredentials: true
        }
      ).then((response) => {
        this.$emit('apiPasswordSetResponse', response.data)
      }).catch((error) => {
        this.apiPasswordSetParseError(error)
      })
    },
    performApiPasswordSetP (obj) {
      return new Promise((resolve, reject) => {
        this.$on('apiPasswordSetResponse', (response) => {
          resolve(response)
        })
        this.$on('apiPasswordSetError', (error) => {
          reject(error)
        })

        this.performApiPasswordSet(obj)
      })
    }
  }
}
