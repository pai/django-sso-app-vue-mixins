/**
 * @mixin
 */

import ParseErrorMixin from './ParseErrorMixin'
import CredentialsTypeMixin from './CredentialsTypeMixin'


export default {
  mixins: [
    ParseErrorMixin,
    CredentialsTypeMixin
  ],
  props: {
    userCredentialsAsEmailOnly: {
      type: Boolean,
      default: false
    },
	checkUserExistenceUrl: {
      type: String,
      default: '/api/v1/auth/check-user/'
    }
  },
  data () {
    return {
      userDoesNotExistError: this.gettext('Account not found, please') + ' <a href="/signup/" tabindex="-1">' + this.gettext('SIGN UP') + '</a>.',
      userAlreadyExistMessage: this.gettext('already taken, please') + ' <a href="/login/" tabindex="-1">' + this.gettext('LOGIN') + '</a>.'
    }
  },
  methods: {
    checkUserExistence (userCredentials, credentialsType) {
      let params = {}
      let credentialsError = null

      if(!credentialsType) {
        credentialsType = 'login'
      }

      if (this.credentialsTypeIsValid(userCredentials, credentialsType)) {
        params[credentialsType] = userCredentials
      } else {
        if (credentialsType === 'email') {
          credentialsError = this.invalidEmailError
        } else if (credentialsType == 'username') {
          credentialsError = this.invalidUsernameError
        } else {
          credentialsError = this.invalidCredentialsError
        }
      }

      if (credentialsError) {
        this.$emit('checkUserExistenceError', credentialsError)
      } else {
        this.$http.get(
          this.checkUserExistenceUrl,
          {
            params: params
          }
        ).then((response) => {
          let msg = ''
          if (credentialsType == 'email') {
            msg += 'E-mail '
          } else if (credentialsType == 'username') {
            msg += this.gettext('Username') + ' '
          } else {
            msg += this.gettext('Credentials') + ' '
          }
          msg += this.userAlreadyExistMessage

          this.$emit('userExists', msg)

        }).catch((error) => {
          let message = this.parseError(error)

          if (error.response) {
            let status = error.response.status

            if (status === 404) {
              this.$emit('userDoesNotExists', this.userDoesNotExistError)
            } else if (status === 400) {
              this.$emit('checkUserExistenceValidationError', error.response.data[0])
            } else {
              this.$emit('checkUserExistenceError', message)
            }
          } else {
            this.$emit('checkUserExistenceError', message)
          }
        })
      }
    },
    checkUserExistenceP (userCredentials, credentialsType) {
      return new Promise((resolve, reject) => {
        this.$on('userExists', (response) => {
          resolve(response)
        })
        this.$on('checkUserExistenceError', (error) => {
          reject(error)
        })
        this.$on('userDoesNotExists', (error) => {
          reject(error)
        })

        this.checkUserExistence(userCredentials, credentialsType)
      })
    },
    checkUserNotExistenceP (userCredentials, credentialsType) {
      return new Promise((resolve, reject) => {
        this.$on('userExists', (result) => {
          reject(result)
        })
        this.$on('checkUserExistenceError', (error) => {
          reject(error)
        })
        this.$on('checkUserExistenceValidationError', (error) => {
          reject(error)
        })
        this.$on('userDoesNotExists', (result) => {
          resolve(result)
        })

        this.checkUserExistence(userCredentials, credentialsType)
      })
    }
  }
}
