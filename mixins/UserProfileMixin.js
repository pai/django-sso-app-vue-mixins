/**
 * @mixin
 */

export default {
  props: {
    /**
    * Profile object
    */
    externalUserProfile: {
      type: Object,
      default: null
    }
  },
  data () {
    return {
      userProfile: this.externalUserProfile
    }
  },
  computed: {
    userProfileUsername () {
      if (this.userProfile && this.userProfile.username) {
        return this.userProfile.username
      }
    },
    userProfileEmail () {
      if (this.userProfile) {
        return this.userProfile.email
      }
    },
    userProfilePicture () {
      if (this.userProfile) {
        return this.userProfile.picture
      }
    }
  },
  watch: {
    externalUserProfile (n) {
      if (n) {
        this.userProfile = n
      }
    }
  }
}
