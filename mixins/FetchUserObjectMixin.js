/**
 * @mixin
 */

import ParseErrorMixin from './ParseErrorMixin'


export default {
  mixins: [
    ParseErrorMixin
  ],
  data () {
    return {
      fetchUserObjectPermittedErrors: '401,403',
    }
  },
  methods: {
    parseFetchUserObjectError (error) {
      let message = this.parseError(error)

      if (error.response) {
        if (this.fetchUserObjectPermittedErrors.indexOf(error.response.status) > -1) {
          this.$emit('userIsAnonymous', '/login/')
        } else {
          this.$emit('errorFetchingUserObject', message)
        }
      } else {
        this.$emit('errorFetchingUserObject', message)
      }
    },
    fetchUserObject (url) {
      this.$emit('performingFetchUserObject', this.name)

      this.$http.get(
        url,
        {
          withCredentials: true
        }
      ).then((response) => {
        this.$emit('userObjectFetched', response.data)
      }).catch((error) => {
        this.parseFetchUserObjectError(error)
      })
    },
    fetchUserObjectP (url) {
      return new Promise((resolve, reject) => {
        this.$on('userObjectFetched', (response) => {
          resolve(response)
        })
        this.$on('errorFetchingUserObject', (error) => {
          reject(error)
        })
        this.$on('userIsAnonymous', (error) => {
          reject('/login/')
        })

        this.fetchUserObject(url)
      })
    }
  }
}
