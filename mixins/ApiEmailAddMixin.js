/**
 * @mixin
 */

import ParseErrorMixin from './ParseErrorMixin'

export default {
  mixins: [
    ParseErrorMixin
  ],
  props: {
    apiEmailAddUrl: {
      type: String,
      default: '/api/v1/auth/email/'
    }
  },
  methods: {
    performApiEmailAdd (email) {
      this.$emit('performingApiEmailAdd')

      this.$http.post(
        this.apiEmailAddUrl,
        {
		  email: email
		},
        {
          withCredentials: true
        }
      ).then((response) => {
        this.$emit('apiEmailAddResponse', response.data)
      }).catch((error) => {
        this.$emit('apiEmailAddError', this.parseError(error))
      })
    },
    performApiEmailAddP (email) {
      return new Promise((resolve, reject) => {
        this.$on('apiEmailAddResponse', (response) => {
          resolve(response)
        })
        this.$on('apiEmailAddError', (error) => {
          reject(error)
        })

        this.performApiEmailAdd(email)
      })
    }
  }
}
