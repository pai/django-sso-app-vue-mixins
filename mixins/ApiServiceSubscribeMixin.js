/**
 * @mixin
 */

import ParseErrorMixin from './ParseErrorMixin'

export default {
  mixins: [
    ParseErrorMixin
  ],
  methods: {
    performApiServiceSubscribe (url) {
      this.$emit('performingApiServiceSubscribe')

      this.$http.post(
        url,
        {},
        {
          withCredentials: true
        }
      ).then((response) => {
        this.$emit('apiServiceSubscribeResponse', response.data)
      }).catch((error) => {
        this.$emit('apiServiceSubscribeError', this.parseError(error))
      })
    },
    performApiServiceSubscribeP (url) {
      return new Promise((resolve, reject) => {
        this.$on('apiServiceSubscribeResponse', (response) => {
          resolve(response)
        })
        this.$on('apiServiceSubscribeError', (error) => {
          reject(error)
        })

        this.performApiServiceSubscribe(url)
      })
    }
  }
}
