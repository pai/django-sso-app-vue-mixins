/**
 * @mixin
 */

import ParseErrorMixin from './ParseErrorMixin'

export default {
  mixins: [
    ParseErrorMixin
  ],
  methods: {
    performApiProfileUpdate (url, obj) {
      this.$emit('performingApiProfileUpdate')

      this.$http.patch(
        url,
        obj,
        {
          withCredentials: true
        }
      ).then((response) => {
        this.$emit('apiProfileUpdateResponse', response.data)
      }).catch((error) => {
        this.$emit('apiProfileUpdateError', this.parseError(error))
      })
    },
    performApiProfileUpdateP (url, obj) {
      return new Promise((resolve, reject) => {
        this.$on('apiProfileUpdateResponse', (response) => {
          resolve(response)
        })
        this.$on('apiProfileUpdateError', (error) => {
          reject(error)
        })

        this.performApiProfileUpdate(url, obj)
      })
    }
  }
}
