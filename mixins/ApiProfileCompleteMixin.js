/**
 * @mixin
 */

import ParseErrorMixin from './ParseErrorMixin'

export default {
  mixins: [
    ParseErrorMixin
  ],
  methods: {
    performApiProfileComplete (url, obj) {
      this.$emit('performingApiProfileComplete')

      this.$http.patch(
        url, 
        obj,
        {
          withCredentials: true
        }
      ).then((response) => {
        this.$emit('apiProfileCompleteResponse', response.data)
      }).catch((error) => {
        this.$emit('apiProfileCompleteError', this.parseError(error))
      })
    },
    performApiProfileCompleteP (url, obj) {
      return new Promise((resolve, reject) => {
        this.$on('apiProfileCompleteResponse', (response) => {
          resolve(response)
        })
        this.$on('apiProfileCompleteError', (error) => {
          reject(error)
        })

        this.performApiProfileComplete(url, obj)
      })
    }
  }
}
