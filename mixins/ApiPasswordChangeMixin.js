/**
 * @mixin
 */

import ParseErrorMixin from './ParseErrorMixin'

export default {
  mixins: [
    ParseErrorMixin
  ],
  props: {
    apiPasswordChangeUrl: {
      type: String,
      default: '/api/v1/auth/password/change/'
    }
  },
  methods: {
    apiPasswordChangeParseError (error) {
      let parsedError = this.parseError(error)
      let msg = parsedError

      if (typeof error === 'object') {
        msg = this.gettext('Check password.')
      }

      this.$emit('apiPasswordChangeError', msg)
    },
    performApiPasswordChange (obj) {
      this.$emit('performingApiPasswordChange')

      this.$http.post(
        this.apiPasswordChangeUrl,
        obj,
        {
          withCredentials: true
        }
      ).then((response) => {
        this.$emit('apiPasswordChangeResponse', response.data)
      }).catch((error) => {
        this.apiPasswordChangeParseError(error)
      })
    },
    performApiPasswordChangeP (obj) {
      return new Promise((resolve, reject) => {
        this.$on('apiPasswordChangeResponse', (response) => {
          resolve(response)
        })
        this.$on('apiPasswordChangeError', (error) => {
          reject(error)
        })

        this.performApiPasswordChange(obj)
      })
    }
  }
}
