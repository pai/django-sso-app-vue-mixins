/**
 * @mixin
 */

import ParseErrorMixin from './ParseErrorMixin'

export default {
  mixins: [
    ParseErrorMixin
  ],
  props: {
    apiPasswordResetKeyUrl: {
      type: String,
      default: '/api/v1/auth/password/reset/key/'
    }
  },
  methods: {
    performApiPasswordResetKey (uidb36, key, password) {
      this.$emit('performingApiPasswordResetKey', this.name)

      let url = this.apiPasswordResetKeyUrl

      let params = {
        uidb36: uidb36,
        key: key,
        password1: password,
        password2: password
      }

      this.$http.post(
        url,
        params,
        {}
      ).then((response) => {
        this.$emit('apiPasswordResetKeyResponse', response.data)
      }).catch((error) => {
        this.$emit('apiPasswordResetKeyError', this.parseError(error))
      })
    },
    performApiPasswordResetKeyP (uidb36, key, password) {
      return new Promise((resolve, reject) => {
        this.$on('apiPasswordResetKeyResponse', (response) => {
          resolve(response)
        })
        this.$on('apiPasswordResetKeyError', (error) => {
          reject(error)
        })

        this.performApiPasswordResetKey(uidb36, key, password)
      })
    }
  }
}
