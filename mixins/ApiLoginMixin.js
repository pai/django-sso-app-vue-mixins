/**
 * @mixin
 */

import ParseErrorMixin from './ParseErrorMixin'
import NextUrlMixin from './NextUrlMixin'


export default {
  mixins: [
    ParseErrorMixin,
    NextUrlMixin
  ],
  props: {
	apiLoginUrl: {
      type: String,
      default: '/api/v1/auth/login/'
    }
  },
  data () {
    return {
      passepartoutRedirectUrl: null
    }
  },
  methods: {
    performApiLogin (login, password, fingerprint) {
      this.$emit('performingApiLogin')

      let params = {
        login: login,
        password: password,
        fingerprint: fingerprint
      }

      let url = this.apiLoginUrl

      if (this.nextUrl && this.nextUrl.length && this.nextUrl !== '/') {
        url += '?next=' + this.nextUrl
      }

      this.$http.post(
        url,
        params,
        {}
      ).then((response) => {
        this.passepartoutRedirectUrl = response.data.redirect_url

        this.$emit('apiLoginResponse', response.data)
      }).catch((error) => {
        let message = this.parseError(error)

        let credentialsLoginError = (message === this.gettext('The e-mail address and/or password you specified are not correct.')) ||
                                    (message === 'The e-mail address and/or password you specified are not correct.') ||
                                    (message === this.gettext('The username and/or password you specified are not correct.')) ||
                                    (message === 'The username and/or password you specified are not correct.')

        if (credentialsLoginError) {
          // console.log('apiLoginCredentialsError')
          message += ' ' + this.gettext('Would you <a href="/password/reset/" alt="reset password" tabindex="-1">RESET YOUR PASSWORD</a>?')
          this.$emit('apiLoginCredentialsError', message)
        }

        this.$emit('apiLoginError', message)
      })
    },
    performApiLoginP (login, password, fingerprint) {
      return new Promise((resolve, reject) => {
        this.$on('apiLoginResponse', (response) => {
          resolve(response)
        })
        this.$on('apiLoginError', (error) => {
          reject(error)
        })

        this.performApiLogin(login, password, fingerprint)
      })
    }
  }
}
