/**
 * @mixin
 */

import ParseErrorMixin from './ParseErrorMixin'


export default {
  mixins: [
    ParseErrorMixin
  ],
  props: {
    userProfileUrl: {
      type: String,
      default: '/api/v1/auth/user/'
    }
  },
  data () {
    return {
      fetchUserProfilePermittedErrors: '401,403',
    }
  },
  methods: {
    parseFetchUserProfileError (error) {
      let message = this.parseError(error)

      if (error.response) {
        if (this.fetchUserObjectPermittedErrors.indexOf(error.response.status) > -1) {
          this.$emit('userIsAnonymous', '/login/')
        } else {
          this.$emit('errorFetchingUserProfile', message)
        }
      } else {
        this.$emit('errorFetchingUserProfile', message)
      }
    },
    fetchUserProfile () {
      this.$emit('performingFetchUserProfile', this.name)

      this.$http.get(
        this.userProfileUrl,
        {
          withCredentials: true
        }
      ).then((response) => {
        this.$emit('userProfileFetched', response.data)
      }).catch((error) => {
        this.parseFetchUserProfileError(error)
      })
    },
    fetchUserProfileP (url) {
      return new Promise((resolve, reject) => {
        this.$on('userProfileFetched', (response) => {
          resolve(response)
        })
        this.$on('errorFetchingUserProfile', (error) => {
          reject(error)
        })
        this.$on('userIsAnonymous', (error) => {
          reject(error)
        })

        this.fetchUserProfile(url)
      })
    }
  }
}
