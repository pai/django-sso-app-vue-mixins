/**
 * @mixin
 */

import ParseErrorMixin from './ParseErrorMixin'
import NextUrlMixin from './NextUrlMixin'


export default {
  mixins: [
    ParseErrorMixin,
    NextUrlMixin
  ],
  props: {
	apiSignupUrl: {
      type: String,
      default: '/api/v1/auth/signup/'
    }
  },
  methods: {
    performApiSignup (obj) {
      this.$emit('performingApiSignup')

      this.$http.post(
        this.apiSignupUrl,
        obj,
        {}
      ).then((response) => {
        this.$emit('apiSignupResponse', response.data)
      }).catch((error) => {
        this.$emit('apiSignupError', this.parseError(error))
      })
    },
    performApiSignupP (obj) {
      return new Promise((resolve, reject) => {
        this.$on('apiSignupResponse', (response) => {
          resolve(response)
        })
        this.$on('apiSignupError', (error) => {
          reject(error)
        })

        this.performApiSignup(obj)
      })
    }
  }
}
